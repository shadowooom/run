命令行指引

你还可以按照以下说明从你的电脑中上传现有文件或项目。

Git 全局设置

git config --global user.name "shadowooom"
git config --global user.email "shadowooom@qq.com"

创建一个新仓库

git clone https://gitcode.com/shadowooom/run.git
cd run
echo "# run" >> README.md
git add README.md
git commit -m "add README"
git branch -m main
git push -u origin main

推送现有的文件

cd existing_folder
git init
git remote add origin https://gitcode.com/shadowooom/run.git
git add .
git commit -m "Initial commit"
git branch -m main
git push -u origin main

推送现有的 Git 仓库

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitcode.com/shadowooom/run.git
git push -u origin --all
git push -u origin --tags